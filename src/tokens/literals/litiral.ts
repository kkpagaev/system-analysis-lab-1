import {Token} from '../../lexer';
import {Reader} from '../../reader';
import {fn} from './function';
import {identifier} from './identifier';
import {keyword} from './keyword';

export function litiral(reader: Reader): Token | null {
  let char;
  let value = '';
  while ((char = reader.peek()) && char.match(/[a-zA-Z0-9]/)) {
    value += char;
    reader.next();
  }
  if (value.length === 0) {
    return null;
  }
  const position = reader.position() - value.length;
  // always return a token beacuse of identifier that can be returned with an error
  const token =
    keyword(value, position) ||
    fn(value, position) ||
    identifier(value, position);

  return token;
}
