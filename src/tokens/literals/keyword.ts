import {Token} from '../../lexer';

import {Reader} from '../../reader';

const functions = ['for', 'const', 'let', 'var', 'while', 'return', 'if'];
export function keyword(value: string, position: number): Token | null {
  if (!functions.includes(value)) {
    return null;
  }
  return {
    type: 'keyword',
    position: position,
    length: value.length,
    value: value,
  };
}
