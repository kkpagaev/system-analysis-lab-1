import {Token} from '../lexer';
import {Reader} from '../reader';

export function EOF(reader: Reader): Token {
  return {
    type: 'EOF',
    position: reader.position(),
    length: 0,
  };
}
