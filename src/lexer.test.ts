import {describe, expect, test} from '@jest/globals';
import {lexer, Token} from './lexer';

describe('Lexer', () => {
  test('should throw an error', () => {
    const result = [...lexer('31x23')];
    expect(result).toEqual([
      {type: 'number', position: 0, length: 2, value: '31'},
      {
        type: 'identifier',
        position: 2,
        length: 3,
        value: 'x23',
        error: true,
      },
      {type: 'EOF', position: 5, length: 0},
    ]);
  });

  test('numbers and opertator should pass', () => {
    const code = '31 -23 + 123';
    expect(() => [...lexer(code)]).not.toThrow();
  });

  test('numbers and opertator should pass', () => {
    const code = '3 / 123  ( 12 ) * 3';
    expect(() => [...lexer(code)]).not.toThrow();
  });

  test('should be comment', () => {
    const code = '# 123 + 123';
    const tokens = [...lexer(code)];
    expect(tokens[0].type).toBe('comment');
  });

  test('should be an error', () => {
    const code = 'kparah2aiev';
    const tokens = [...lexer(code)];
    expect(tokens[0].error).toBe(true);
  });

  test('should be pancuation', () => {
    const code = '(testing 123 555 karahaiev);';
    const tokens = [...lexer(code)];
    expect(tokens[0].type).toBe('punctuation');
    expect(tokens[9].type).toBe('punctuation');
  });

  test('should be keyword', () => {
    const code = '(testing return 555 karahaiev);';
    const tokens = [...lexer(code)];
    expect(tokens[3].type).toBe('keyword');
  });

  test('should be all spaces', () => {
    const code = '   ';
    const tokens = [...lexer(code)];
    expect(tokens[0].type).toBe('whitespace');
    expect(tokens[1].type).toBe('whitespace');
    expect(tokens[2].type).toBe('whitespace');
  });
});
